#modern.ie boxes

Vagrantfile for multiple modern.ie Vagrant boxes.  
  
Learn more at http://joecod.es/modernie-vagrant-boxes/  
I left out some combos of win/ie versions. For a complete list of available boxes, see https://atlas.hashicorp.com/modernIE

##Requirements

[Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://vagrantup.com).

##Usage

List the available virtual machines

```bash
vagrant status
```

Start a new virtual machine

```bash
vagrant up ie11
```

Start multiple virtual machines at once

```bash
vagrant up ie10 ie11 edge
```

Suspend virtual machines (name is optional, skip to suspend all running machines)

```bash
vagrant suspend ie11
```

Destroy virtual machines (name is optional, skip to destroy all existing machines) 

```bash
vagrant destroy ie11
```